//
//  TabBarViewController.h
//  Oye Driver
//
//  Created by Sujan on 7/10/17.
//  Copyright © 2017 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UITabBarController<UITabBarControllerDelegate>


-(void)logOutFromTabbar;

@end
