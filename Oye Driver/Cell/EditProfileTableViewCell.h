//
//  EditProfileTableViewCell.h
//  Shathi
//
//  Created by Sujan on 6/5/17.
//  Copyright © 2017 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UITextField *userInfoTextField;

@end
